from abc import ABC
import base64
import sys
import argparse
import zipfile
import typing as t
import xml.etree.ElementTree as ET


def print_and_exit(message, **kwargs):
    print(message, file=sys.stderr, **kwargs)
    exit(1)


def init_argparse():
    parser = argparse.ArgumentParser(
        description="Extract hashcat-ready hash from ODF file",
        usage="%(prog)s --infile path",
    )
    parser.add_argument("-i", "--infile", required=True,
                        help="path of the ODF file")
    parser.add_argument(
        "-o",
        "--outfile",
        default="odf.hash",
        help="path to save hash to. default: odf.hash",
    )
    return parser


class SubstringMatcher(ABC):
    """
    Checks if any key in matchdict is found in string it was initialised with.
    If so, return it.
    """

    match_dict: dict
    _repr: str

    def __init__(self, string):
        for key, value in self.match_dict.items():
            if key.lower() in string.lower():
                self._repr = value
                return
        else:
            raise ValueError("No match found")

    def __str__(self):
        return str(self._repr)


class ChecksumTypeMatcher(SubstringMatcher):
    match_dict = {"SHA1": 0, "SHA256": 1}


class AlgorithmNameMatcher(SubstringMatcher):
    match_dict = {"Blowfish CFB": 0, "aes256-cbc": 1}


def process_manifest(manifest: bytes) -> dict:
    namespace = "{urn:oasis:names:tc:opendocument:xmlns:manifest:1.0}"

    root = ET.fromstring(manifest)
    for entry in root:
        if entry.attrib[namespace + "full-path"] == "content.xml":
            context_file_entry = entry
            break

    encryption_data = context_file_entry[0]

    data = {
        "checksum-type": encryption_data.get(namespace + "checksum-type"),
        "checksum": encryption_data.get(namespace + "checksum"),
    }

    attrs_to_collect = [  # because fuck xpath
        "algorithm-name",
        "initialisation-vector",
        "key-size",
        "iteration-count",
        "salt",
    ]
    for entry in encryption_data:
        for attr in attrs_to_collect.copy():
            if value := entry.get(namespace + attr):
                data[attr] = value
                attrs_to_collect.remove(attr)

    if attrs_to_collect:
        print_and_exit(
            (
                f"Couldn't extract these attributes from manifest.xml: {', '.join(attrs_to_collect)}"
                "You sure the file is encrypted?"
            )
        )

    return data


def get_hex(val: t.Union[str, bytes]) -> str:
    if isinstance(val, str):
        val = base64.b64decode(val)
    return val.hex()


def get_len(val: str) -> str:
    return str(int(len(val) / 2))


def process_data(data: dict) -> dict:
    try:
        data["algorithm-name"] = str(AlgorithmNameMatcher(data["algorithm-name"]))
    except ValueError:
        print_and_exit(
            f"Cannot parse attribute {data['algorithm-name']=} from manifest.xml",
        )

    try:
        data["checksum-type"] = str(ChecksumTypeMatcher(data["checksum-type"]))
    except ValueError:
        print_and_exit(
            f"Cannot parse attribute {data['checksum-type']=} from manifest.xml",
        )

    data["checksum"] = get_hex(data["checksum"])

    data["initialisation-vector"] = get_hex(data["initialisation-vector"])
    data["iv_len"] = get_len(data["initialisation-vector"])

    data["salt"] = get_hex(data["salt"])
    data["salt_len"] = get_len(data["salt"])

    return data


def main():
    parser = init_argparse()
    args = parser.parse_args()

    try:
        zip_file = zipfile.ZipFile(args.infile, "r")
        manifest = zip_file.open("META-INF/manifest.xml").read()
        content = zip_file.open("content.xml").read(1024)

    except (zipfile.BadZipFile, KeyError):
        print_and_exit("Cannot read this file.")

    data = process_manifest(manifest)
    data = process_data(data)
    content = get_hex(content)

    result = "*".join(
        (
            "$odf$",
            data["algorithm-name"],
            data["checksum-type"],
            data["iteration-count"],
            data["key-size"],
            data["checksum"],
            data["iv_len"],
            data["initialisation-vector"],
            data["salt_len"],
            data["salt"],
            "0",
            content,
        )
    )
    with open(args.outfile, "w") as f:
        f.write(result)
    print("Format recognised, saved to file!")

    if data["algorithm-name"] == data["checksum-type"] == "1":
        print("Hashcat mode 18400: Open Document Format (ODF) 1.2 (SHA-256, AES)")

    elif data["algorithm-name"] == data["checksum-type"] == "0":
        print("Hashcat mode 18600: Open Document Format (ODF) 1.1 (SHA-1, Blowfish)")

    else:
        print("Hashcat mode (⊙＿⊙'): ¯\_(ツ)_/¯ Format (WTF) 1.0 (ヽ(ಠ_ಠ)ノ, ¯\_ಠ_ಠ_/¯)")
        exit(1)


if __name__ == "__main__":
    main()
    exit(0)
